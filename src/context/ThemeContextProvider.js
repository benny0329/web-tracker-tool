import React, { useState } from 'react';

import { deepmerge } from '@mui/utils';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';

import ThemeContext from './ThemeContext';
import { GroundhogTheme } from '../theme';

const defaultTheme = GroundhogTheme;

const ThemeContextProvider = ({ children }) => {
  const [darkMode, setDarkMode] = useState(false);
  const darkTheme = {
    palette: {
      mode: darkMode ? 'dark' : 'light',
    },
  };
  const mergeTheme = deepmerge(defaultTheme, darkTheme);
  const theme = responsiveFontSizes(createTheme(mergeTheme));

  return (
    <ThemeContext.Provider value={{ theme, darkMode, setDarkMode }}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeContextProvider;
