const GroundhogTheme = {
  palette: {
    mode: 'light',
    primary: {
      main: '#6CB31B',
    },
    secondary: {
      main: '#ABB31B',
    },
    error: {
      main: '#f44d4d',
    },
    warning: {
      main: '#ffd500',
    },
    info: {
      main: '#4dc4ff',
    },
    success: {
      main: '#4dffc4',
    },
  },
};

export default GroundhogTheme;
