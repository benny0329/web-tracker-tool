import React from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';

import DraftsIcon from '@mui/icons-material/Drafts';
import SendIcon from '@mui/icons-material/Send';
import ModeEditIcon from '@mui/icons-material/ModeEdit';

import { styled } from '@mui/material/styles';

import Layout from './components/layout';
import Header from './components/header';
import Drawer from './components/drawer';

import Home from './pages/home';
import MonacoEditor from './pages/monaco-editor';
import NoMatch from './pages/no-match';

import ThemeContextProvider from './context/ThemeContextProvider';

const ContentWrapper = styled('main')(({ theme }) => ({
  flexGrow: 1,
  padding: theme.spacing(2),
}));

const PAGES_CONFIG = [
  {
    primary: 'Monaco Editor',
    icon: <ModeEditIcon />,
    to: '/monaco-editor',
  },
  {
    primary: 'Sent mail',
    icon: <SendIcon />,
    to: '/sent-mail',
  },
  {
    primary: 'Drafts',
    icon: <DraftsIcon />,
    to: '/drafts',
  },
];

const ROUTES_CONFIG = [
  {
    exact: true,
    path: '/',
    children: Home,
  },
  {
    path: '/monaco-editor',
    children: MonacoEditor,
  },
];

function App() {
  const [selectedListItem, setSelectedListItem] = React.useState(null);

  const handleListItemClick = (event, item) => {
    setSelectedListItem(item);
  };

  return (
    <ThemeContextProvider>
      <Router>
        <Layout>
          <Box sx={{ display: 'flex' }}>
            <Header
              title={'Web Tracker Tool'}
              onListItemClick={handleListItemClick}
            />
            <Drawer
              items={PAGES_CONFIG}
              selectedListItem={selectedListItem}
              onListItemClick={handleListItemClick}
            />
            <ContentWrapper>
              <Toolbar />
              <Switch>
                {ROUTES_CONFIG.map((routeProps, index) => (
                  <Route
                    key={routeProps.path}
                    children={<routeProps.children />}
                    {...routeProps}
                  />
                ))}
                <Route path="*">
                  <NoMatch />
                </Route>
              </Switch>
            </ContentWrapper>
          </Box>
        </Layout>
      </Router>
    </ThemeContextProvider>
  );
}

export default App;
