import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';

import ThemeContext from '../context/ThemeContext';

const Layout = ({ children }) => {
  const { theme } = useContext(ThemeContext);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

Layout.displayName = 'Layout';
Layout.propTypes = {
  children: PropTypes.node,
};

export default Layout;
