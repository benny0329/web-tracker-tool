import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';

import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

const drawerWidth = 240;

const ClippedDrawer = ({ items, selectedListItem, onListItemClick }) => {
  const renderPages = (items) => {
    return items.map((item) => {
      const { primary, icon, children, to, ...pageProps } = item;

      return (
        <ListItem
          button
          component={RouterLink}
          to={to}
          key={primary}
          selected={selectedListItem === primary}
          onClick={(event) => onListItemClick(event, primary)}
          {...pageProps}
        >
          {icon && <ListItemIcon>{React.cloneElement(icon)}</ListItemIcon>}
          <ListItemText primary={primary} />
        </ListItem>
      );
    });
  };

  return (
    <Drawer
      variant="permanent"
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' },
      }}
    >
      <Toolbar />
      <Box sx={{ overflow: 'auto' }}>
        <List component="nav" aria-labelledby="nested-list">
          {renderPages(items)}
        </List>
      </Box>
    </Drawer>
  );
};

export default ClippedDrawer;
