import React from 'react';
import { useLocation } from 'react-router-dom';

function NoMatch() {
  let location = useLocation();

  return (
    <React.Fragment>
      No match for <code>{location.pathname}</code>
    </React.Fragment>
  );
}

export default NoMatch;
