import * as React from 'react';
import Typography from '@mui/material/Typography';

function Home() {
  return (
    <Typography align="center" variant="h4" component="h1" gutterBottom>
      Groundhog Tracking App
    </Typography>
  );
}

export default Home;
